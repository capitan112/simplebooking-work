//
//  ViewController.m
//  LSWeekView-Example
//
//  Created by Christoph Zelazowski on 6/16/14.
//  Copyright (c) 2014 Lumen Spark. All rights reserved.
//

#import "ViewController.h"
#import "LSWeekView.h"
#import "CACollectionView.h"

@interface ViewController ()
@property (nonatomic, weak) IBOutlet LSWeekView *weekView;
@property (nonatomic, strong) NSDateFormatter *dateFormatter;
@property (nonatomic, strong) NSDateFormatter *dayFormatter;
@property (nonatomic, weak) CACollectionView *containerViewController;
@property (nonatomic, strong) NSString *selectedDay;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleDataChange)
                                                 name:@"getDataFromServer"
                                               object:nil];
    self.view.tintColor = [UIColor redColor];
    self.dateFormatter = [[NSDateFormatter alloc] init];
    self.dayFormatter = [[NSDateFormatter alloc] init];
    NSString *dateFormat = [NSDateFormatter dateFormatFromTemplate:@"EEEE MMMM d yyyy" options:0 locale:[NSLocale currentLocale]];
    NSString *dayFormat = [NSDateFormatter dateFormatFromTemplate:@"MM dd yyyy" options:0 locale:nil];
    [self.dateFormatter setDateFormat:dateFormat];
    [self.dayFormatter setDateFormat:dayFormat];
    self.selectedDay = [self.dayFormatter stringFromDate:self.weekView.selectedDate];
    self.containerViewController.requiredDay = self.selectedDay;
//    NSLog(@"viewDidLoad ");
    __weak typeof(self) weakSelf = self;
    self.weekView.didChangeSelectedDateBlock = ^(NSDate *selectedDate) {
        [weakSelf updateDate];
    };
    [self updateDate];
}

- (void)viewDidUnload {
  [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)updateDate {
//    NSLog(@"update date");
    self.selectedDay = [self.dayFormatter stringFromDate:self.weekView.selectedDate];
    self.containerViewController.requiredDay = self.selectedDay;
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    [self.containerViewController getDataFromServer];
    [self.containerViewController.collectionView reloadData];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

- (IBAction)todayButtonAction:(id)sender {
    NSDate *date = [NSDate date];
    if ([self.weekView setSelectedDate:date animated:YES] == NO) {
      [self.weekView animateSelectedDateMarker];
    }
    [self updateDate];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"embedContainer"]) {
        self.containerViewController.requiredDay = self.selectedDay;
        self.containerViewController = segue.destinationViewController;
        [self addChildViewController:_containerViewController];
        [_containerViewController didMoveToParentViewController:self];
//        NSLog(@"segue working");
    }
}

- (void)handleDataChange {
    dispatch_async(dispatch_get_main_queue(), ^ {
        [self.containerViewController.collectionView reloadData];
    });
}


@end
