#import "CACollectionViewCell.h"
#import "CACollectionHeaderView.h"
#import "CARPCConnection.h"
#import "CACollectionView.h"
#import "CANetworkIndictor.h"

static NSString *const headerIdentifier = @"HeaderIdentifier";
static NSString *const reuseCellIdentifier = @"CellIdentifier";

@interface CACollectionView () {
    int numberOfItemsInSection;
    CARPCConnection *rpcConnection;
    NSDictionary *shedduleTimeDict;
    NSArray *sortedKeyHoursArray;
    int selectedPath;
    CANetworkIndictor *netIndicator;
}

@property (strong, nonatomic) NSString *currentSegueIdentifier;
@property (assign, nonatomic) BOOL transitionInProgress;


@end

@implementation CACollectionView

- (void)viewDidLoad {
    [super viewDidLoad];
    numberOfItemsInSection = 5;
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{//    });
    rpcConnection = [[CARPCConnection alloc] init];
//    netIndicator = [CANetworkIndictor defaultIndicator];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


#pragma mark getting data from server

- (void)getDataFromServer {

     shedduleTimeDict = nil;
     dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
     [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;

//     NSLog(@"getDataFromServer day %@", self.requiredDay);
        NSDictionary *getStartTimeMatrix = @{
                                                @"method" : @"getStartTimeMatrix",
                                                @"params" : @[_requiredDay, _requiredDay, @(1), @(1) ]
                                             };
        NSDictionary *startTimeTemp = [rpcConnection createRequestWithDict:getStartTimeMatrix];
        if (startTimeTemp) {
        
            NSArray *startTimeData = [[NSArray alloc]initWithArray:startTimeTemp[@"result"][_requiredDay]];
            NSMutableDictionary *shedduleTime = [[NSMutableDictionary alloc] init];
            for (NSString *time in startTimeData) {
                NSString *truncatedString = [time substringToIndex:[time length] - 3];
                [shedduleTime setValue:@"available" forKey:truncatedString];
            }
            NSDictionary *getReservedTime = @{
                                                @"method" : @"getReservedTime",
                                                @"params" : @[_requiredDay, _requiredDay, @(1), @(1)]
                                              };
            NSDictionary *reservedTimeTemp = [rpcConnection createRequestWithDict:getReservedTime];
            NSArray *reservedTimeData = [[NSArray alloc]initWithArray:reservedTimeTemp[@"result"][_requiredDay]];
            for (NSDictionary *busyTime in reservedTimeData) {
                for(NSDictionary *busyHours in busyTime[@"events"]) {
                    [shedduleTime setValue:@"busy" forKey:busyHours[@"from"]];
                }
            }
            NSArray* keys = [shedduleTime allKeys];
            sortedKeyHoursArray = [keys sortedArrayUsingComparator:^(id a, id b) {
                return [a compare:b options:NSNumericSearch];
            }];
//            NSLog(@"shedduleTime %@", shedduleTime);
            shedduleTimeDict = [shedduleTime copy];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"getDataFromServer" object:self];
        }
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    });

}

#pragma mark - delegate method

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [shedduleTimeDict count];
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    CACollectionViewCell *cell = (CACollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:reuseCellIdentifier forIndexPath:indexPath];
    NSString *hoursKey = sortedKeyHoursArray[indexPath.row];
    NSString *hoursAviability = shedduleTimeDict[hoursKey];
    if ([hoursAviability isEqualToString:@"available"]) {
        cell.enable = YES;
    } else {
        cell.enable = NO;
    }
    cell.layer.masksToBounds = NO;
    cell.layer.cornerRadius = 8; // rounded corners
    cell.layer.shadowOffset = CGSizeMake(-15, 20);
    cell.layer.shadowRadius = 5;
    cell.layer.shadowOpacity = 0.8;
    cell.layer.shadowOffset = CGSizeMake(6, 6);
    cell.layer.shadowPath = [UIBezierPath bezierPathWithRect:cell.bounds].CGPath;
    //тень
    CGColorRef cellColor = [UIColor colorWithRed:9/255.0f green:9/255.0f blue:9/255.0f alpha:1.0f].CGColor;
    cell.layer.shadowColor = cellColor;
    if (!cell.enable) {
        //цвет busy
        cell.backgroundColor = [UIColor colorWithRed:246/255.0f green:145/255.0f blue:160/255.0f alpha:1.0f];
        cell.cellLabel.text = @"busy";
    } else {
        //цвет, для выбора
        cell.backgroundColor = [UIColor colorWithRed:196/255.0f green:255/255.0f blue:137/255.0f alpha:1.0f];
        cell.cellLabel.text = hoursKey;
    }
//    if ([sortedKeyHoursArray count] == 0) {
//        NSLog(@"no data %@",sortedKeyHoursArray);
//    }

    return cell;
}

//#pragma mark - cell delegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSString *hoursKey = sortedKeyHoursArray[indexPath.row];
    NSString *hoursAviability = shedduleTimeDict[hoursKey];
    if ([hoursAviability isEqualToString:@"available"]) {
        selectedPath = indexPath.row;
//        NSLog(@"hoursKey %d", selectedPath);
    } else {
        selectedPath = - 1;
    }
}

//#pragma mark header delegate

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    CACollectionHeaderView *headerView = (CACollectionHeaderView *)[collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:headerIdentifier forIndexPath:indexPath];
    if (indexPath.section == 0) {
        [headerView setText:@"Tap the button to book time"];
    } else {
        [headerView setText:[NSString stringWithFormat:@"10:00-11:00"]];
    }
    return headerView;
}

#pragma mark - dynamic cell size

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    int spaceBetweenCell = (300/numberOfItemsInSection) - 54;
    int minSpaceBetweenCell = 6;
    
    return spaceBetweenCell > 0 ? spaceBetweenCell : minSpaceBetweenCell;
}

#pragma mark - booking

- (IBAction)booked:(id)sender {
//    NSLog(@"booking");
    if (selectedPath < 0 || !selectedPath) {
        UIAlertView * alert =[[UIAlertView alloc ] initWithTitle:@"Unspecified time"
                                                         message:@"Please choose time for booking"
                                                        delegate:self
                                               cancelButtonTitle:@"Ok"
                                               otherButtonTitles: nil];
        [alert show];
        return;
    }
    NSString *requiredTime = sortedKeyHoursArray[selectedPath];
//    NSLog(@"requiredTime %@", requiredTime);
    NSDictionary *book  = @{
                            @"method" : @"book",
                            @"params" : @[@(1), @(1), _requiredDay, requiredTime, @{@"name": @"Dad", @"email": @"plrs@mail.ru", @"phone": @"+380504191155"}, @"true" ]
                            };
    NSDictionary *bookResult = [rpcConnection createRequestWithDict:book];
    NSLog(@"bookResult %@", bookResult);
    if (bookResult[@"error"]) {
        UIAlertView * alert =[[UIAlertView alloc ] initWithTitle:@"Current time has choosed"
                                                         message:@"Please choose time for booking"
                                                        delegate:self
                                               cancelButtonTitle:@"Ok"
                                               otherButtonTitles: nil];
        [alert show];
        [self getDataFromServer];
        return;
        
    }
    UIAlertView * alert =[[UIAlertView alloc ] initWithTitle:@"Booked"
                                                     message:[NSString stringWithFormat: @"You have booked %@",requiredTime]
                                                    delegate:self
                                           cancelButtonTitle:@"Ok"
                                           otherButtonTitles: nil];
    [alert show];
    [self getDataFromServer];
    dispatch_async(dispatch_get_main_queue(), ^ {
        [self.collectionView reloadData];
    });
}

@end
