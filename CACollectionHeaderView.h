#import <UIKit/UIKit.h>

@interface CACollectionHeaderView : UICollectionReusableView

@property (nonatomic, copy) NSString *text;

@property (weak, nonatomic) IBOutlet UILabel *headerLabel;

@end
