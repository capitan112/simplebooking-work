//
//  CANetworkIndictor.m
//  LSWeekView-Example
//
//  Created by Капитан on 03.09.14.
//  Copyright (c) 2014 Lumen Spark. All rights reserved.
//

#import "CANetworkIndictor.h"

@interface CANetworkIndictor() {
    NSUInteger _numberOfActivities;
}

@end

@implementation CANetworkIndictor

+ (instancetype)defaultIndicator {
    static dispatch_once_t onceToken;
    static CANetworkIndictor *_defaultIndicator;
    dispatch_once(&onceToken, ^{
        _defaultIndicator = [[CANetworkIndictor alloc] init];
    });
    return _defaultIndicator;
}

- (void)startActivity {
    @synchronized(self) {
        _numberOfActivities++;
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    }
}

-(void)stopActivity {
    @synchronized(self) {
        if (_numberOfActivities) {
            _numberOfActivities--;
        }
        [UIApplication sharedApplication].networkActivityIndicatorVisible = (_numberOfActivities > 0);
    }
}

@end
