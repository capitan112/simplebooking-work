//
//  CACollectionView.h
//  LSWeekView-Example
//
//  Created by Капитан on 01.09.14.
//  Copyright (c) 2014 Lumen Spark. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CACollectionView : UICollectionViewController

@property (strong, nonatomic) NSString *requiredDay;

- (void)getDataFromServer;

@end
