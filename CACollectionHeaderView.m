#import "CACollectionHeaderView.h"

@implementation CACollectionHeaderView {
    UILabel *textLabel;
}

- (void)prepareForReuse {
    [super prepareForReuse];
    [self setText:@""];
}

- (void)setText:(NSString *)text {
    _text = [text copy];
    [_headerLabel setText:text];
}

@end
