//
//  CANetworkIndictor.h
//  LSWeekView-Example
//
//  Created by Капитан on 03.09.14.
//  Copyright (c) 2014 Lumen Spark. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CANetworkIndictor : NSObject

+ (instancetype)defaultIndicator;

- (void)startActivity;
- (void)stopActivity;


@end
